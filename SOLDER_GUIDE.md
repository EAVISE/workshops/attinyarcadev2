# Solder guide
This guide shows the different steps required to solder the EAVISE ATTinyArcadeV2 (with D-pad) PCB.
## Part list
You should have received the following parts, please thoroughly check. The battery (CR2032) will be given to you at the end of the workshop. During development of the software, the PCB will be powered through the board that is used to program it. We solder the components from lowest to heighest package.
<ul>
<li>PCB</li>
<li>Piezo buzzer</li>
<li>ATTiny85 microcontroller</li>
<li>IC socket</li>
<li>6 pin header</li>
<li>Battery holder</li>
<li>100 nF capacitor</li>
<li>3 mm LED</li>
<li>Slide switch</li>
<li>5 push buttons</li>
<li>OLED screen</li>
<li>CR2032 Battery</li>
<li>8 resistors: 2 x 82 KOhm, 2 x 33 KOhm, 2 x 22 KOhm, 1 x 10 KOhm, 1 x 560 Ohm</li>
</ul> 
<div>
<img src="img/part_list.jpg" width="500" style="display:block;">
</div>

## Step 1: solder jumpers
Some OLED screen headers have the VCC and GND connections swapped. For this reason, the ATTinyArcade PCB has two solder jumpers on the first two header pins. Since the header of our OLED screen has GND / VDD as pin 1 and 2 respectively, we need to solder the solder jumpers on both the top layer and bottom layer to connect the middle and right connection (see images below).
<div>
<img src="img/1_jumper1.jpg" width="500"  style="display: block;">
</div>
<div>
<img src="img/2_jumper2.jpg" width="500">
</div>

## Step 2: solder resistors
Next, solder the eight resistors to the board. Important: make sure to solder the correct resistance values to their correct location! The color bands and position are given below. You can use a multi-meter if required. Please note the orientation (for esthetic reasons we mostly aim to solder the tolerance band (gold) in the same direction).
<ul>
<li>R3 & R6: 82 KOhm (grey, red, orange, gold)</li>
<li>R2 & R5: 33 KOhm (orange, orange, orange, gold)</li>
<li>R1 & R4: 22 Kohm (red, red, orange, gold)</li>
<li>R7: 10 KOhm (brown, black, orange, gold)</li>
<li>R8: 560 Ohm (green, blue, brown, gold)</li>
</ul>
<div>
<img src="img/3_resistors.jpg" width="500">
</div>

## Step 3: solder the power switch
<div>
<img src="img/4_power_switch.jpg" width="500">
</div>

## Step 4: solder power LED
Please note the orientation such that anode and cathode are connected correctly. The long leg is the anode, and should be connected to the resistor.
<div>
<img src="img/5_led.jpg" width="500"> 
</div>

## Step 5: solder the IC socket
Note that the notch is pointed to the left. Make sure the IC socket sits flush with the PCB.
<div>
<img src="img/6_ic_holder.jpg" width="500">
</div>

## Step 6: solder the five push buttons
<div>
<img src="img/7_push_buttons.jpg" width="500">
</div>

## Step 7: solder the capacitor
The 100nF capacitor acts as a decoupling capacitor (for more information, see [here](https://learn.sparkfun.com/tutorials/capacitors/application-examples)).
<div>
<img src="img/8_capacitor.jpg" width="500">
</div>

## Step 8: solder the battery clip
This battery clip is soldered from the top layer. Note that the solder connections will be covered by the OLED screen (hence, that's why they are soldered first).
<div>
<img src="img/9_battery_clip.jpg" width="500">
</div>

## Step 9: solder the OLED screen
The OLED screen consists of 128x64 pixels, an I2C interface and measures 0.96 inch. Try to solder it such that it fits nice, straight and level with the PCB.
<div>
<img src="img/10_oled.jpg" width="500">
</div>

## Step 10: solder the piezzo buzzer
For this project we've used a passive piezzo buzzer ([this](https://be.farnell.com/tdk/ps1440p02bt/piezoelectronic-buzzer-3v-4khz/dp/3212600) one). For more information on the difference between an active and a passive buzzer, see [here](https://www.kailitech.net/What-is-the-difference-between-active-and-passive-buzzer-id3985781.html). The polarity is not important.
<div>
<img src="img/10_2_buzzer.jpg" width="500">
</div>

## Step 11: solder the programming header
This header is used to program the bootloader and is also soldered on the top layer. It might be required to slightly trim the pins of the push button underneath the header, or to solder the header under a slight angle.
<div>
<img src="img/11_connector.jpg" width="500">
</div>

## Step 12: electrical testing of the PCB
Now we are going to perform some basic electrical testing (without microcontroller!), to see if you soldered everything correct. First, plug in the CR2032 battery and turn on the console. The power LED should light up.

Use a multimeter in the correct range (DC voltages of around 5V order of magnitude, most meters have a 20V DC position). Measure the voltage between the ground (pin 4) and the remaining pins of the IC socket. The image below shows how the pins are connected to the microcontroller. Pin 8 is Vcc. For pins 1, 5 and 7 the voltages change depending on which buttons are pressed. Make sure these values match the values from the table below. This is a good check to see if the resistors are soldered correctly. Note that depending on the charge state of the battery, the exact voltages might slightly differ from the ones below. 
<div>
<img src="img/electrical_testing.png" width="700">
</div>

## Final product
If the electrical test went well, turn off the power. Finally, place the ATTiny85 microcontroller in its socket. Again, note that the notch points to the left. If everything went well, the final PCB should look (and at the end work) like this:
<div>
<img src="img/12_final.jpg" width="500">
</div>
<div>
<img src="img/12_2_final.jpg" width="500">
</div>
