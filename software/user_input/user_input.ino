/* 2023 EAVISE - User input

*/

#include "font6x8.h"
#include "oled_driver.h"

void setup() {
  ssd1306_init();
  ssd1306_fillscreen(0x00);

  // Write code here to initialise user input
}

void loop() {
  // Draw border
  draw_border();

  // Write code here to show ADC values and keypresses on the OLED screen
  
  delay(50);
}

void draw_border() {

  // Draw top line
  ssd1306_setpos(0, 0);
  ssd1306_send_data_start();
  for (int i = 0; i < 128; i++) {
    ssd1306_send_byte(0x01); // 0b00000001
  }
  ssd1306_send_data_stop();

  // Draw bottom line
  ssd1306_setpos(0, 7);
  ssd1306_send_data_start();
  for (int i = 0; i < 128; i++) {
    ssd1306_send_byte(0x80); // 0b10000000
  }
  ssd1306_send_data_stop();

  // Draw side lines
  for (int i = 0; i < 8; i++) {
    // Left border
    ssd1306_setpos(0, i);
    ssd1306_send_data_start();
    ssd1306_send_byte(0xFF); // 0b11111111
    ssd1306_send_data_stop();

    // Right border
    ssd1306_setpos(127, i);
    ssd1306_send_data_start();
    ssd1306_send_byte(0xFF); // 0b11111111
    ssd1306_send_data_stop();
  }
}
