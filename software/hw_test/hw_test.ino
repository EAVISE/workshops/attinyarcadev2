// EAVISE WORKSHOP ATTINY85 ARCADE
// TESTPROGRAM
// Kristof Van Beeck
//
// based on:
//
//                >>>>>  TINY JOYPAD TESTER  GPL v3 <<<<
//                   >>>>>  FOR TINYJOYPAD rev2  <<<<
//                      Programmer: Daniel C 2021
//            Contact EMAIL: electro_l.i.b@tinyjoypad.com
//              official website: www.tinyjoypad.com
//       or  https://sites.google.com/view/arduino-collection

//  TINY JOYPAD TESTER is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Includes
#include "TESTMODE_PIC.h"
#include "font6x8.h"

// Defines for user IO
#define TINYJOYPAD_LEFT  (analogRead(A0)>=750)&&(analogRead(A0)<950)
#define TINYJOYPAD_RIGHT (analogRead(A0)>500)&&(analogRead(A0)<750)
#define TINYJOYPAD_DOWN (analogRead(A1)>=750)&&(analogRead(A1)<950)
#define TINYJOYPAD_UP  (analogRead(A1)>500)&&(analogRead(A1)<750)
#define BUTTON_DOWN (digitalRead(0)==0)
#define BUTTON_UP (digitalRead(0)==1)

// Defines for OLED screen
#define DIGITAL_WRITE_HIGH(PORT) PORTB |= (1 << PORT)
#define DIGITAL_WRITE_LOW(PORT) PORTB &= ~(1 << PORT)
// ---------------------  // Vcc, Pin 1 on SSD1306 Board
// ---------------------  // GND, Pin 2 on SSD1306 Board
#define SSD1306_SCL   PB4 // SCL, Pin 3 on SSD1306 Board
#define SSD1306_SDA   PB3 // SDA, Pin 4 on SSD1306 Board
#define SSD1306_SA    0x78  // Slave address

// Function prototypes
void Sound(uint8_t freq_, uint8_t dur);
uint8_t blitzSprite(int8_t xPos, int8_t yPos, uint8_t xPASS, uint8_t yPASS, uint8_t FRAME, const uint8_t *SPRITES);
int8_t RecupeLineY(int8_t Valeur);
uint8_t RecupeDecalageY(uint8_t Valeur);
uint8_t SplitSpriteDecalageY(uint8_t decalage, uint8_t Input, uint8_t UPorDOWN);
void draw_logo(void);

void ssd1306_init(void);
void ssd1306_xfer_start(void);
void ssd1306_xfer_stop(void);
void ssd1306_send_byte(uint8_t byte);
void ssd1306_send_command(uint8_t command);
void ssd1306_send_data_start(void);
void ssd1306_send_data_stop(void);
void ssd1306_setpos(uint8_t x, uint8_t y);
void ssd1306_fillscreen(uint8_t fill_Data);
void ssd1306_char_f6x8(uint8_t x, uint8_t y, const char ch[]);

// Public variables
uint8_t X_JOY, Y_JOY;

void setup() {
  // Setup for IO
  pinMode(0, INPUT);    // ACTION
  digitalWrite(1, LOW); // Piezzo buzzer set low
  pinMode(1, OUTPUT);   // Piezo buzzer
  pinMode(A0, INPUT);   // LEFT / RIGHT
  pinMode(A1, INPUT);   // UP / DOWN

  // Setup for OLED screen
  ssd1306_init();
  ssd1306_fillscreen(0x00);

  draw_logo();

  delay(2000);
  ssd1306_fillscreen(0x00);
}

void loop() {
  for (uint8_t t = 2; t < 250; t++) {
    Sound(t, 1);
  }
  X_JOY = 29;
  Y_JOY = 30;
  Tiny_Flip_JOYTESTER();
  while (1) {
    if (TINYJOYPAD_RIGHT) {
      X_JOY = (X_JOY < 47) ? X_JOY + 6 : 47;
    } else if (TINYJOYPAD_LEFT) {
      X_JOY = (X_JOY > 11) ? X_JOY - 6 : 11;
    } else {
      if (X_JOY < 29) {
        X_JOY += 6;
      }
      if (X_JOY > 29) {
        X_JOY -= 6;
      }
    }
    if (TINYJOYPAD_DOWN) {
      Y_JOY = (Y_JOY < 48) ? Y_JOY + 6 : 48;
    } else if (TINYJOYPAD_UP) {
      Y_JOY = (Y_JOY > 12) ? Y_JOY - 6 : 12;
    } else {
      if (Y_JOY < 30) {
        Y_JOY += 6;
      }
      if (Y_JOY > 30) {
        Y_JOY -= 6;
      }
    }
    Tiny_Flip_JOYTESTER();
  }
}

uint8_t JOYTEST_JOYTESTER(uint8_t xPASS, uint8_t yPASS) {
  return pgm_read_byte(&JOYTEST[xPASS + (yPASS * 128)]);
}

uint8_t boutton_JOYTESTER(uint8_t xPASS, uint8_t yPASS) {
  if (BUTTON_DOWN) {
    return (0xff - blitzSprite(97, 20, xPASS, yPASS, 0, boutton));
  } else {
    return 0xff;
  }
}

uint8_t Joy_JOYTESTER(uint8_t xPASS, uint8_t yPASS) {
  return (blitzSprite(X_JOY, Y_JOY, xPASS, yPASS, 0, Joy));
}

void Tiny_Flip_JOYTESTER(void) {
  uint8_t y, x;
  for (y = 0; y < 8; y++) {

    // Set position
    ssd1306_send_command(0xb0 + y);
    ssd1306_send_command(0xb00);
    ssd1306_send_command(0xb10);

    // Start data transfer
    ssd1306_send_data_start();

    for (x = 0; x < 128; x++) {
      ssd1306_send_byte(Joy_JOYTESTER(x, y) | (JOYTEST_JOYTESTER(x, y)&boutton_JOYTESTER(x, y)));
    }
  }
  ssd1306_send_data_stop();
}

void Sound(uint8_t freq_, uint8_t dur) {
  for (uint8_t t = 0; t < dur; t++) {
    if (freq_ != 0) PORTB = PORTB | 0b00000010;
    for (uint8_t t = 0; t < (255 - freq_); t++) {
      _delay_us(1);
    }
    PORTB = PORTB & 0b11111101;
    for (uint8_t t = 0; t < (255 - freq_); t++) {
      _delay_us(1);
    }
  }
}

uint8_t blitzSprite(int8_t xPos, int8_t yPos, uint8_t xPASS, uint8_t yPASS, uint8_t FRAME, const uint8_t *SPRITES) {
  uint8_t OUTBYTE;
  uint8_t WSPRITE = (pgm_read_byte(&SPRITES[0]));
  uint8_t HSPRITE = (pgm_read_byte(&SPRITES[1]));
  uint8_t Wmax = ((HSPRITE * WSPRITE) + 1);
  uint8_t PICBYTE = FRAME * (Wmax - 1);
  int8_t RECUPELINEY = RecupeLineY(yPos);
  if ((xPASS > ((xPos + (WSPRITE - 1)))) || (xPASS < xPos) || ((RECUPELINEY > yPASS) || ((RECUPELINEY + (HSPRITE)) < yPASS))) {
    return 0x00;
  }
  int8_t SPRITEyLINE = (yPASS - (RECUPELINEY));
  uint8_t SPRITEyDECALAGE = (RecupeDecalageY(yPos));
  uint8_t ScanA = (((xPASS - xPos) + (SPRITEyLINE * WSPRITE)) + 2);
  uint8_t ScanB = (((xPASS - xPos) + ((SPRITEyLINE - 1) * WSPRITE)) + 2);
  if (ScanA > Wmax) {
    OUTBYTE = 0x00;
  } else {
    OUTBYTE = SplitSpriteDecalageY(SPRITEyDECALAGE, pgm_read_byte(&SPRITES[ScanA + (PICBYTE)]), 1);
  }
  if ((SPRITEyLINE > 0)) {
    uint8_t OUTBYTE2 = SplitSpriteDecalageY(SPRITEyDECALAGE, pgm_read_byte(&SPRITES[ScanB + (PICBYTE)]), 0);
    if (ScanB > Wmax) {
      return OUTBYTE;
    } else {
      return OUTBYTE | OUTBYTE2;
    }
  } else {
    return OUTBYTE;
  }
}

uint8_t SplitSpriteDecalageY(uint8_t decalage, uint8_t Input, uint8_t UPorDOWN) {
  if (UPorDOWN) {
    return Input << decalage;
  }
  return Input >> (8 - decalage);
}

int8_t RecupeLineY(int8_t Valeur) {
  return (Valeur >> 3);
}

uint8_t RecupeDecalageY(uint8_t Valeur) {
  return (Valeur - ((Valeur >> 3) << 3));
}

void draw_logo() {
  // Loop over pages
  // x-pos in pixels, y-pos in pages (0-7)
  // EAVISE logo is 100 pixels x 24 pixels (i.e. 3 pages)
  for (byte lxn = 0; lxn < 3; lxn++) {
    ssd1306_setpos(14, lxn + 2);
    ssd1306_send_data_start();
    for (byte lxn2 = 0; lxn2 < 100; lxn2++) {
      ssd1306_send_byte(pgm_read_byte(&EAVISE_logo[100 * lxn + lxn2]));
    }
    ssd1306_send_data_stop();
  }

  ssd1306_char_f6x8(38, 5, "KU LEUVEN");
}

void ssd1306_init(void) {
  DDRB |= (1 << SSD1306_SDA); // Set port as output
  DDRB |= (1 << SSD1306_SCL); // Set port as output

  // Optional
  // ssd1306_send_command(0xAE); // display off

  // ssd1306_send_command(0x00); // Set lower column start address
  // ssd1306_send_command(0x10); // Set higher column start address
  // ssd1306_send_command(0x40); // Set display line start
  
  // ssd1306_send_command(0x81); // Set Contrast Control
  // ssd1306_send_command(0xCF); // value(default = 0x7F)

  // By default, screen is upsidedown
  ssd1306_send_command(0xA1); // Set Segment Re-map (default 0xA0)
  ssd1306_send_command(0xC8); // Set COM Output Scan Direction (default 0xC0)

  // Optional
  // ssd1306_send_command(0xA6); // Set Normal/Inverse Display
  
  // ssd1306_send_command(0xA8); // Set multiplex ratio
  // ssd1306_send_command(0x3F); // value
  
  // ssd1306_send_command(0xD3); // Set display offset
  // ssd1306_send_command(0x00); // value
  
  // ssd1306_send_command(0xD5); // Set Display Clock Divide Ratio/ Oscillator Frequency
  // ssd1306_send_command(0x80); // value
  
  // ssd1306_send_command(0xD9); // Set Pre-charge Period
  // ssd1306_send_command(0xF1); // value
  
  // ssd1306_send_command(0xDA); // Set COM Pins Hardware Configuration
  // ssd1306_send_command(0x12); // value
  
  // ssd1306_send_command(0xDB); // Set VCOMH Deselect Level
  // ssd1306_send_command(0x40); // value
  
  // ssd1306_send_command(0x20); // Set Memory Addressing Mode 
  // ssd1306_send_command(0x02); // value

  // Turn charge pump on, since battery voltage is too low to power screen by default
  ssd1306_send_command(0x8D); // Charge Pump Setting
  ssd1306_send_command(0x14); // Enable charge pump (disabled by default)
  
  // Optional  
  // ssd1306_send_command(0xA4); // Entire display on
  
  ssd1306_send_command(0xAF); // Turn on OLED panel
}

void ssd1306_xfer_start(void) {
  DIGITAL_WRITE_HIGH(SSD1306_SCL);  // Set to HIGH
  DIGITAL_WRITE_HIGH(SSD1306_SDA);  // Set to HIGH
  DIGITAL_WRITE_LOW(SSD1306_SDA);   // Set to LOW
  DIGITAL_WRITE_LOW(SSD1306_SCL);   // Set to LOW
}

void ssd1306_xfer_stop(void) {
  DIGITAL_WRITE_LOW(SSD1306_SCL);   // Set to LOW
  DIGITAL_WRITE_LOW(SSD1306_SDA);   // Set to LOW
  DIGITAL_WRITE_HIGH(SSD1306_SCL);  // Set to HIGH
  DIGITAL_WRITE_HIGH(SSD1306_SDA);  // Set to HIGH
}

void ssd1306_send_byte(uint8_t byte) {
  uint8_t i;
  for (i = 0; i < 8; i++)
  {
    if ((byte << i) & 0x80)
      DIGITAL_WRITE_HIGH(SSD1306_SDA);
    else
      DIGITAL_WRITE_LOW(SSD1306_SDA);

    DIGITAL_WRITE_HIGH(SSD1306_SCL);
    DIGITAL_WRITE_LOW(SSD1306_SCL);
  }
  DIGITAL_WRITE_HIGH(SSD1306_SDA);
  DIGITAL_WRITE_HIGH(SSD1306_SCL);
  DIGITAL_WRITE_LOW(SSD1306_SCL);
}

void ssd1306_send_command(uint8_t command) {
  ssd1306_xfer_start();
  ssd1306_send_byte(SSD1306_SA);  // Slave address, SA0=0
  ssd1306_send_byte(0x00);  // write command
  ssd1306_send_byte(command);
  ssd1306_xfer_stop();
}

void ssd1306_send_data_start(void) {
  ssd1306_xfer_start();
  ssd1306_send_byte(SSD1306_SA);
  ssd1306_send_byte(0x40);  //write data
}

void ssd1306_send_data_stop(void) {
  ssd1306_xfer_stop();
}

void ssd1306_setpos(uint8_t x, uint8_t y)
{
  ssd1306_xfer_start();
  ssd1306_send_byte(SSD1306_SA);  //Slave address,SA0=0
  ssd1306_send_byte(0x00);  //write command

  ssd1306_send_byte(0xb0 + y);
  ssd1306_send_byte(((x & 0xf0) >> 4) | 0x10); // |0x10
  ssd1306_send_byte(x & 0x0f);

  ssd1306_xfer_stop();
}

void ssd1306_fillscreen(uint8_t fill_Data) {
  uint8_t m, n;
  for (m = 0; m < 8; m++)
  {
    ssd1306_send_command(0xb0 + m); //page0-page1
    ssd1306_send_command(0x00);   //low column start address
    ssd1306_send_command(0x10);   //high column start address
    ssd1306_send_data_start();
    for (n = 0; n < 128; n++)
    {
      ssd1306_send_byte(fill_Data);
    }
    ssd1306_send_data_stop();
  }
}

void ssd1306_char_f6x8(uint8_t x, uint8_t y, const char ch[]) {
  uint8_t c, i, j = 0;
  while (ch[j] != '\0')
  {
    c = ch[j] - 32;
    if (x > 126)
    {
      x = 0;
      y++;
    }
    ssd1306_setpos(x, y);
    ssd1306_send_data_start();
    for (i = 0; i < 6; i++)
    {
      ssd1306_send_byte(pgm_read_byte(&ssd1306xled_font6x8[c * 6 + i]));
    }
    ssd1306_send_data_stop();
    x += 6;
    j++;
  }
}
