//
// This file works for TinyJoypad compatible devices.
//
// If not compiled for ATTiny85 (meaning __AVR_ATtiny85__ is not defined),
// generic functions are used instead of direct port access, which
// makes it possible to use an Arduino or Mega2560 (or many others)
// for debugging with serial output or even hardware breakpoints.
//

#include <Arduino.h>
#include "TinyJoypadUtils.h"

#define DIGITAL_WRITE_HIGH(PORT) PORTB |= (1 << PORT)
#define DIGITAL_WRITE_LOW(PORT) PORTB &= ~(1 << PORT)
#define SSD1306_SA    0x78  // Slave address
#define SSD1306_SCL   PB4 // SCL, Pin 3 on SSD1306 Board
#define SSD1306_SDA   PB3 // SDA, Pin 4 on SSD1306 Board

// buffered analog joystick inputs
uint16_t analogJoystickX;
uint16_t analogJoystickY;


/*-------------------------------------------------------*/
// function for initializing the TinyJoypad (ATtiny85) and other microcontrollers
void InitTinyJoypad()
{
  // configure A0, A3 and D1 as input
  SOUND_PORT_DDR &= ~( ( 1 << PB5) | ( 1 << PB3 ) | ( 1 << PB1 ) );
  // configure A2 (aka SOUND_PIN) as output
  SOUND_PORT_DDR |= ( 1 << SOUND_PIN );
}

/*-------------------------------------------------------*/
bool isLeftPressed()
{
  uint16_t inputX = analogRead( LEFT_RIGHT_BUTTON );
  return ( ( inputX >= 750 ) && ( inputX < 950 ) );
}

/*-------------------------------------------------------*/
bool isRightPressed()
{
  uint16_t inputX = analogRead( LEFT_RIGHT_BUTTON );
  return ( ( inputX > 500 ) && ( inputX < 750 ) );
}

/*-------------------------------------------------------*/
bool isUpPressed()
{
  uint16_t inputY = analogRead( UP_DOWN_BUTTON );
  return ( ( inputY > 500 ) && ( inputY < 750 ) );
}

/*-------------------------------------------------------*/
bool isDownPressed()
{
  uint16_t inputY = analogRead( UP_DOWN_BUTTON );
  return ( ( inputY >= 750 ) && ( inputY < 950 ) );
}

/*-------------------------------------------------------*/
bool isFirePressed()
{
  return ( digitalRead( FIRE_BUTTON ) == 0 );
}

/*-------------------------------------------------------*/
// wait until all buttons are released
void waitUntilButtonsReleased()
{
  while ( isLeftPressed() || isRightPressed() || isUpPressed() || isDownPressed() || isFirePressed() );
}

/*-------------------------------------------------------*/
// wait until all buttons are released and wait a little delay
void waitUntilButtonsReleased( const uint8_t delayTime )
{
  waitUntilButtonsReleased();
  _delay_ms( delayTime );
}

/*-------------------------------------------------------*/
// read analog joystick inputs into internal variables
void readAnalogJoystick()
{
  analogJoystickX = analogRead( LEFT_RIGHT_BUTTON );
  analogJoystickY = analogRead( UP_DOWN_BUTTON );
}

/*-------------------------------------------------------*/
bool wasLeftPressed()
{
  return ( ( analogJoystickX >= 750 ) && ( analogJoystickX < 950 ) );
}

/*-------------------------------------------------------*/
bool wasRightPressed()
{
  return ( ( analogJoystickX > 500 ) && ( analogJoystickX < 750 ) );
}

/*-------------------------------------------------------*/
bool wasUpPressed()
{
  return ( ( analogJoystickY > 500 ) && ( analogJoystickY < 750 ) );
}

/*-------------------------------------------------------*/
bool wasDownPressed()
{
  return ( ( analogJoystickY >= 750 ) && ( analogJoystickY < 950 ) );
}

/*-------------------------------------------------------*/
uint16_t getAnalogValueX()
{
  return ( analogJoystickX );
}

/*-------------------------------------------------------*/
uint16_t getAnalogValueY()
{
  return ( analogJoystickY );
}

/*-------------------------------------------------------*/
void __attribute__ ((noinline)) _variableDelay_us( uint8_t delayValue )
{
  while ( delayValue-- != 0 )
  {
    _delay_us( 1 );
  }
}

/*-------------------------------------------------------*/
// This code was originaly borrowed from Daniel C's Tiny-invaders :)
// Code optimization by sbr
void Sound( const uint8_t freq, const uint8_t dur )
{
  for ( uint8_t t = 0; t < dur; t++ )
  {
    if ( freq != 0 ) {
      SOUND_PORT = SOUND_PORT | ( 1 << SOUND_PIN);
    }
    _variableDelay_us( 255 - freq );
    SOUND_PORT = SOUND_PORT & ~( 1 << SOUND_PIN );
    _variableDelay_us( 255 - freq );
  }
}

/*-------------------------------------------------------*/
void InitDisplay()
{
  DDRB |= (1 << SSD1306_SDA); // Set port as output
  DDRB |= (1 << SSD1306_SCL); // Set port as output
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //According to the graphics library of your choice, insert the code here to initialize the oled ssd1306 screen//
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // By default, screen is upsidedown
  TinyOLED_send_command(0xA1); // Set Segment Re-map (default 0xA0)
  TinyOLED_send_command(0xC8); // Set COM Output Scan Direction (default 0xC0)

  //ssd1306_send_command(0x81); // Set Contrast Control
  //ssd1306_send_command(0xCF); // value(default = 0x7F)

  // Turn charge pump on, since battery voltage is too low to power screen by default
  TinyOLED_send_command(0x8D); // Charge Pump Setting
  TinyOLED_send_command(0x14); // Enable charge pump (disabled by default)

  // Turn on OLED panel
  TinyOLED_send_command(0xAF);
}

void TinyOLED_send_command(uint8_t command) {
  TinyOLED_Begin();
  TinyOLED_Send(SSD1306_SA);  // Slave address, SA0=0
  TinyOLED_Send(0x00);  // write command
  TinyOLED_Send(command);
  TinyOLED_End();
}

void TinyOLED_Begin(void) {
  DIGITAL_WRITE_HIGH(SSD1306_SCL);  // Set to HIGH
  DIGITAL_WRITE_HIGH(SSD1306_SDA);  // Set to HIGH
  DIGITAL_WRITE_LOW(SSD1306_SDA);   // Set to LOW
  DIGITAL_WRITE_LOW(SSD1306_SCL);   // Set to LOW
}

void TinyOLED_End(void) {
  DIGITAL_WRITE_LOW(SSD1306_SCL);   // Set to LOW
  DIGITAL_WRITE_LOW(SSD1306_SDA);   // Set to LOW
  DIGITAL_WRITE_HIGH(SSD1306_SCL);  // Set to HIGH
  DIGITAL_WRITE_HIGH(SSD1306_SDA);  // Set to HIGH
}

void TinyOLED_Send(uint8_t byte) {
  uint8_t i;
  for (i = 0; i < 8; i++)
  {
    if ((byte << i) & 0x80)
      DIGITAL_WRITE_HIGH(SSD1306_SDA);
    else
      DIGITAL_WRITE_LOW(SSD1306_SDA);

    DIGITAL_WRITE_HIGH(SSD1306_SCL);
    DIGITAL_WRITE_LOW(SSD1306_SCL);
  }
  DIGITAL_WRITE_HIGH(SSD1306_SDA);
  DIGITAL_WRITE_HIGH(SSD1306_SCL);
  DIGITAL_WRITE_LOW(SSD1306_SCL);
}

void TinyOLED_Data_Start(uint8_t y) {
  TinyOLED_send_command(0xb0 + y);
  TinyOLED_send_command(0xb00);
  TinyOLED_send_command(0xb10);

  TinyOLED_Begin();
  TinyOLED_Send(SSD1306_SA);
  TinyOLED_Send(0x40);  //write data
}

/*-------------------------------------------------------*/
// This code will init the display for row <y>
void PrepareDisplayRow( uint8_t y )
{
  // initialize image transfer to segment 'y'
  TinyOLED_send_command(0xb0 + y);
  TinyOLED_Begin();
  TinyOLED_Send(SSD1306_SA);
  TinyOLED_Send(0x40);  //write data
}

/*-------------------------------------------------------*/
void SendPixels( uint8_t pixels )
{
  // send a byte directly to the SSD1306
  TinyOLED_Send(pixels);
}

/*-------------------------------------------------------*/
// This code will finish a row (only on Tiny85)
void FinishDisplayRow()
{
  // this line appears to be optional, as it was never called during the intro screen...
  // but hey, we still have some bytes left ;)
  TinyOLED_End();
}

/*-------------------------------------------------------*/
void DisplayBuffer()
{

}


/*-------------------------------------------------------*/
// Perform a screenshot if
//  [x] enabled and
//  [x] trigger condition met
void CheckForSerialScreenshot()
{
#if !defined(__AVR_ATtiny85__) /* codepath for any Adafruit_SSD1306 supported MCU */
#ifdef _ENABLE_SERIAL_SCREENSHOT_
  if ( _SERIAL_SCREENSHOT_TRIGGER_CONDITION_ )
  {
    // perform the screenshot
    SerialScreenshot();
  }
#endif
#endif
}

///////////////////////////////////////////////////////////////////////////////////
// serial output without clustering the code with #if !defined(__AVR_ATtiny85__)...

/*-------------------------------------------------------*/
void serialPrint( const char *text )
{
#ifdef USE_SERIAL_PRINT
  Serial.print( text );
#endif
}

/*-------------------------------------------------------*/
void serialPrintln( const char *text )
{
#ifdef USE_SERIAL_PRINT
  Serial.println( text );
#endif
}

/*-------------------------------------------------------*/
void serialPrint( const __FlashStringHelper *text )
{
#ifdef USE_SERIAL_PRINT
  Serial.print( text );
#endif
}

/*-------------------------------------------------------*/
void serialPrintln( const __FlashStringHelper *text )
{
#ifdef USE_SERIAL_PRINT
  Serial.println( text );
#endif
}

/*-------------------------------------------------------*/
void serialPrint( const unsigned int number )
{
#ifdef USE_SERIAL_PRINT
  Serial.print( number );
#endif
}

/*-------------------------------------------------------*/
void serialPrintln( const unsigned int number )
{
#ifdef USE_SERIAL_PRINT
  Serial.println( number );
#endif
}

/*-------------------------------------------------------*/
void serialPrint( const int number )
{
#ifdef USE_SERIAL_PRINT
  Serial.print( number );
#endif
}

/*-------------------------------------------------------*/
void serialPrintln( const int number )
{
#ifdef USE_SERIAL_PRINT
  Serial.println( number );
#endif
}
