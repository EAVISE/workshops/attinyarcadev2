//                >>>>>  TINY JOYPAD TESTER  GPL v3 <<<<
//                   >>>>>  FOR TINYJOYPAD rev2  <<<<
//                      Programmer: Daniel C 2021
//            Contact EMAIL: electro_l.i.b@tinyjoypad.com
//              official website: www.tinyjoypad.com
//       or  https://sites.google.com/view/arduino-collection

//  TINY JOYPAD TESTER is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

//  You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//for TINY JOYPAD rev2 (attiny85)
//the code work at 16MHZ internal
//Program the chip with an arduino uno in "Arduino as ISP" mode.

//prototype
void TinyOLED_init(void);
void TinyOLED_Begin(void);
void TinyOLED_End(void);
void TinyOLED_Send(uint8_t byte);
void TinyOLED_send_command(uint8_t command);
void TinyOLED_Data_Start(uint8_t Y);

#define DIGITAL_WRITE_HIGH(PORT) PORTB |= (1 << PORT)
#define DIGITAL_WRITE_LOW(PORT) PORTB &= ~(1 << PORT)
#define SSD1306_SA    0x78  // Slave address
#define SSD1306_SCL   PB4 // SCL, Pin 3 on SSD1306 Board
#define SSD1306_SDA   PB3 // SDA, Pin 4 on SSD1306 Board

void TinyOLED_init(void) {
  DDRB |= (1 << SSD1306_SDA); // Set port as output
  DDRB |= (1 << SSD1306_SCL); // Set port as output
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //According to the graphics library of your choice, insert the code here to initialize the oled ssd1306 screen//
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // By default, screen is upsidedown
  TinyOLED_send_command(0xA1); // Set Segment Re-map (default 0xA0)
  TinyOLED_send_command(0xC8); // Set COM Output Scan Direction (default 0xC0)

  //ssd1306_send_command(0x81); // Set Contrast Control
  //ssd1306_send_command(0xCF); // value(default = 0x7F)

  // Turn charge pump on, since battery voltage is too low to power screen by default
  TinyOLED_send_command(0x8D); // Charge Pump Setting
  TinyOLED_send_command(0x14); // Enable charge pump (disabled by default)

  // Turn on OLED panel
  TinyOLED_send_command(0xAF);
}

void TinyOLED_Begin(void) {
  DIGITAL_WRITE_HIGH(SSD1306_SCL);  // Set to HIGH
  DIGITAL_WRITE_HIGH(SSD1306_SDA);  // Set to HIGH
  DIGITAL_WRITE_LOW(SSD1306_SDA);   // Set to LOW
  DIGITAL_WRITE_LOW(SSD1306_SCL);   // Set to LOW
}

void TinyOLED_End(void) {
  DIGITAL_WRITE_LOW(SSD1306_SCL);   // Set to LOW
  DIGITAL_WRITE_LOW(SSD1306_SDA);   // Set to LOW
  DIGITAL_WRITE_HIGH(SSD1306_SCL);  // Set to HIGH
  DIGITAL_WRITE_HIGH(SSD1306_SDA);  // Set to HIGH
}

void TinyOLED_Send(uint8_t byte) {
  uint8_t i;
  for (i = 0; i < 8; i++)
  {
    if ((byte << i) & 0x80)
      DIGITAL_WRITE_HIGH(SSD1306_SDA);
    else
      DIGITAL_WRITE_LOW(SSD1306_SDA);

    DIGITAL_WRITE_HIGH(SSD1306_SCL);
    DIGITAL_WRITE_LOW(SSD1306_SCL);
  }
  DIGITAL_WRITE_HIGH(SSD1306_SDA);
  DIGITAL_WRITE_HIGH(SSD1306_SCL);
  DIGITAL_WRITE_LOW(SSD1306_SCL);
}

void TinyOLED_send_command(uint8_t command) {
  TinyOLED_Begin();
  TinyOLED_Send(SSD1306_SA);  // Slave address, SA0=0
  TinyOLED_Send(0x00);  // write command
  TinyOLED_Send(command);
  TinyOLED_End();
}

void TinyOLED_Data_Start(uint8_t y) {
  TinyOLED_send_command(0xb0 + y);
  TinyOLED_send_command(0xb00);
  TinyOLED_send_command(0xb10);

  TinyOLED_Begin();
  TinyOLED_Send(SSD1306_SA);
  TinyOLED_Send(0x40);  //write data
}
