/*  2023 Snake game, modified by Kristof Van Beeck - KU Leuven EAVISE, based on:
    2015 UFO Escape game by Ilya Titov.
*/

#include "font6x8.h"
#include "oled_driver.h"
#include "utils.h"

// Defines for user IO
#define ACTION 0 // PB0
#define LEFT  (analogRead(A0)>=750)&&(analogRead(A0)<950)
#define RIGHT (analogRead(A0)>500)&&(analogRead(A0)<750)
#define DOWN (analogRead(A1)>=750)&&(analogRead(A1)<950)
#define UP  (analogRead(A1)>500)&&(analogRead(A1)<750)

// Snake game
void read_user_input(void);
void move_snake(void);
void drop_bait(void);
boolean bait_catched(void);
boolean check_collision(void);
void draw_snake(void);
void draw_bait(void);
void draw_score(void);
void high_score(void);
void render_game(void);

// Snake utils
void draw_startup_screen(void);
void changeSettings(void);
void system_sleep(void);

// Global variables
byte len = 3; // Start length of snake
const int maxLen = 100; // Max length of snake
// The screen buffer
// The game grid consists of a 32x16 grid (=128x64 in 4x4 blocks). The screen buffer consists of a long (= 4 bytes) of length 16, so this can store the entire grid.
unsigned long screenBuffer[16];
boolean selfCollision = false; // When collision, game over
byte xPos[maxLen]; // X-positions of snake segments in game grid
byte yPos[maxLen]; // Y-positions of snake segments in game grid
unsigned long lastFrame = 0; // Time stamp of current frame
int frameDelay = 300; // Initial frame rate (300 ms)
int baitX = 0;
int baitY = 0;
boolean baitDropped = false;
int score = 0; // score - this affects the difficulty of the game

void setup() {
  // Initialise OLED screen
  ssd1306_init();
  ssd1306_fillscreen(0x00);

  // Set directions of all user IO
  pinMode(0, INPUT);    // ACTION
  pinMode(1, OUTPUT);   // Piezo buzzer
  digitalWrite(1, LOW); // Piezzo buzzer set low
  pinMode(A0, INPUT);   // LEFT / RIGHT
  pinMode(A1, INPUT);   // UP / DOWN
}

void loop() {

  resetGame();

  while (selfCollision == false) {

    // See if and which button was pressed
    // Write yourself
    read_user_input();

    // See if new frame needs to be processed
    if (millis() > lastFrame + frameDelay) {
      lastFrame = millis();
      beep(50, 200);

      // Calculate new frame time
      if (score < 100) {
        frameDelay  = 300 - score * 3 ;
      }

      // Move the snake in the right direction, and check if we are on the edge of the screen
      // The game grid is 32 x 16
      // Write yourself
      move_snake();

      // Clear buffer first
      for (int i = 0; i < 16; i++) {
        screenBuffer[i] = 0;
      }

      // Draw snake in screenbuffer
      // Uncomment when required
      // draw_snake();

      // Drop new bait, if needed
      if (baitDropped == false) {
        // Write yourself
        drop_bait();
        baitDropped = true;
      }

      // Check if the bait is catched with the head of the snake, increment length and score
      // Write yourself
      if (bait_catched() == true) {
        baitDropped = false;
      }

      // Draw bait in sceenbuffer
      // Write yourself, use draw_snake() as inspiration
      draw_bait();

      // Check collision
      // Write yourself
      selfCollision = check_collision();

      // Render the 32x16 game grid
      // Given
      render_game();

      // Draw score on bottom right of screen
      // Optional
      draw_score();
    }
  }

  // Game over
  high_score();
  delay(2000);
  system_sleep();
}

void resetGame() {
  // Start screen and fill with zeros
  ssd1306_init();
  ssd1306_fillscreen(0x00);

  // Show startup screen
  // Snake logo is 100 pixels x 48 pixels (i.e. 6 pages)
  draw_startup_screen();

  // X-pos in pixels (0 - 127)
  // Y-pos in line number (0-7)
  ssd1306_char_f6x8(34, 6, "S N A K E");
  delay(200);
  ssd1306_char_f6x8(10, 7, "EAVISE - KU Leuven");
  delay(200);

  // Write yourself, optional
  // Can be used to e.g. mute sound at the beginning of the game, or contrast settings,...
  changeSettings();

  // Play intro music and wait
  playMusic(IntroMusic, 14);
  delay(2000);

  // Reset variables
  len = 3;
  score = 0;
  selfCollision = false;
  frameDelay = 300;
  baitDropped = false;

  // Start position of snake
  // OLED screen resolution 128 x 64 ==> snake part is 4x4 ==> 32 x 16 grid
  xPos[2] = 14;
  yPos[2] = 7;
  xPos[1] = 15;
  yPos[1] = 7;
  xPos[0] = 16;
  yPos[0] = 7;
}

void draw_startup_screen(void)
{
  for (byte lxn = 0; lxn < 6; lxn++) {
    ssd1306_setpos(14, lxn);
    ssd1306_send_data_start();
    for (byte lxn2 = 0; lxn2 < 100; lxn2++) {
      ssd1306_send_byte(pgm_read_byte(&snake_logo[100 * lxn + lxn2]));
    }
    ssd1306_send_data_stop();
  }
}

void changeSettings(void)
{
  // Optional

  // Write yourself
}

void read_user_input(void) {

  // Write yourself
}

void move_snake(void) {

  // Write yourself
}

void draw_snake(void) {
  // Update screenbuffer with position of snake
  for (int snake = len - 1; snake >= 0; snake--) {
    screenBuffer[yPos[snake]] = screenBuffer[yPos[snake]] | ((0UL | B00000001) << (31 - xPos[snake]));
  }
}

void drop_bait(void) {

  // Write yourself
}

boolean bait_catched(void) {

  // Write yourself
  return false;
}

void draw_bait(void) {

  // Write yourself
}

boolean check_collision(void) {

  // Write yourself
  return false;
}

void render_game(void) {

  // Loop over 8 rows (the OLED screen is written per vertical byte - 8x8 = 64 pixels)
  for (byte r = 0; r < 8; r++) {
    ssd1306_setpos(0, r);
    ssd1306_send_data_start();

    // Loop over all columns 32 columns, draw per 4 pixels
    for (byte col = 0; col < 32; col++) {

      // Draw the 4x4 box, based on the information in the screen buffer
      for (byte box = 0; box < 4; box++) {
        ssd1306_send_byte( // draw screen buffer data and screen boundaries
          (screenBuffer[r * 2] >> (31 - col)   & B00000001 ? ((box == 1 || box == 2) && baitX == col && baitY == r * 2 && baitDropped == 1 ? B00001001 : B00001111) : B00000000)
          |
          (screenBuffer[r * 2 + 1] >> (31 - col) & B00000001 ? ((box == 1 || box == 2) && baitX == col && baitY == r * 2 + 1 && baitDropped == 1 ? B10010000 : B11110000) : B00000000)
          |
          (r == 0 ? B00000001 : B00000000) // Draw top border
          |
          (r == 7 ? B10000000 : B00000000) // Draw bottom border
          |
          ((col == 0 && box == 0) || (col == 31 && box == 3) ? B11111111 : B00000000) // Draw left and right border
        );
      }
    }
    ssd1306_send_data_stop();
  }
}

void draw_score() {

  // Optional

  // Write yourself
}

void high_score(void) {

  // Optional
  // Write yourself
  ssd1306_char_f6x8(32, 3, "Game Over");

  for (int i = 0; i < 1000; i++) {
    beep(1, random(0, i * 2));
  }
}

void system_sleep(void) {

  // Optional
  // Write yourself
}
