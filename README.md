# ATTinyArcade workshop
<div align="center">
<img src="img/attiny_arcade_console.png" width="300">
<img src="img/attiny_arcade_console_housing.png" width="250">
</div>
This gitlab contains all documentation, datasheets, schematics, PCB design and code for the ATTinyArcadeV2 workshop :raised_hands:. The goal of this workshop is to develop - including soldering the hardware - a mini hand-held arcade game console based on an ATTiny85 microcontroller from semi-scratch. An OLED screen is used for visualisation. Throughout the course, students are thaught different concepts like PCB design, I2C, interrupts, ADCs, OLED technology and so on. They design a simple game from scratch (snake). At the end of the workshop the students are free to upload any of the following complete games (in ATTiny85 version):

* Q*bert (1982)
* Arkanoid (1986)
* Excite Bike (1984)
* Bomberman (1983)
* Lunar Lander (1979)
* Dugger (1988)
* Frogger (1981)
* TinyGilbert (Platformer - new)
* Space invaders (1978)
* Missile Command (1980)
* Morpion (tic-tac-toe)
* Pacman (1980)
* Pinball (1984)
* Pipeline (1978)
* Plaque Attack (1983)
* Hat Trick (1988)
* Tetris (1984)
* Snake (1976)
* Seaquest (1983)
* Mine Sweeper (1990)
* Dungeon Master (new)

At the end of the workshop, they can take the hardware with them.

# Credits
Although a lot of material has been designed specially for this workshop, credits should be given to the following sources.

Part of the code in this workshop is based on:
* [Andy Jackson - Original ATTiny85 games github repository with additional games](https://github.com/andyhighnumber/Attiny-Arduino-Games)

Most finished games are from:
* [Daniel C. - TinyJoypad](https://www.tinyjoypad.com/tinyjoypad_attiny85)
* [Lorandil](https://github.com/Lorandil)
* [Tscha70](https://github.com/tscha70/)

The PCB design in this workshop is based on:
* [Electronoobs - Workshop inspired by the implementation of Electronoobs](https://electronoobs.com/eng_arduino_tut120.php)

# Course slides
[Click here.](./attiny_arcade_workshop.pdf)

# Solder guide
[Click here.](./SOLDER_GUIDE.md)

# Interesting links
* [Arduino Reference](https://www.arduino.cc/reference/en/)
* [Unofficial-list-of-3rd-party-boards-support-urls](https://github.com/arduino/Arduino/wiki/Unofficial-list-of-3rd-party-boards-support-urls)

# Additional board manager URL
* https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json

<div align="center">
<img src="img/kul_eavise_100.png" width="400">
</div>
